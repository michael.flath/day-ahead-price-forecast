
# DE ----------------------------------------------------------------------

DE <- read_csv("data/DE.csv")

DE_processed <- DE %>%
  convert_column_names_to_snakecase() %>%
  mutate(
    delivery_start = as.POSIXct(substr(prediction_time, 1, 19), origin = "1970-01-01", tz = "CET"),
    forecast = round(price, 2),
    country = "DE"
    ) %>%
  select(delivery_start, country, forecast, actual)



# GB ----------------------------------------------------------------------

GB <- read_csv("data/GB.csv")

GB_processed <- GB %>%
  convert_column_names_to_snakecase() %>%
  mutate(
    delivery_start = as.POSIXct(substr(prediction_time, 1, 19), origin = "1970-01-01", tz = "CET"),
    forecast = round(price, 2),
    country = "GB"
  ) %>%
  select(delivery_start, country, forecast, actual)



# FR ----------------------------------------------------------------------

FR <- read_csv("data/FR.csv")

FR_processed <- FR %>%
  convert_column_names_to_snakecase() %>%
  mutate(
    delivery_start = as.POSIXct(substr(prediction_time, 1, 19), origin = "1970-01-01", tz = "CET"),
    forecast = round(price, 2),
    country = "FR"
  ) %>%
  select(delivery_start, country, forecast, actual)



# NL ----------------------------------------------------------------------

NL <- read_csv("data/NL.csv")

NL_processed <- NL %>%
  convert_column_names_to_snakecase() %>%
  mutate(
    delivery_start = as.POSIXct(substr(prediction_time, 1, 19), origin = "1970-01-01", tz = "CET"),
    forecast = round(price, 2),
    country = "NL"
  ) %>%
  select(delivery_start, country, forecast, actual)



# BE ----------------------------------------------------------------------

BE <- read_csv("data/BE.csv")

BE_processed <- BE %>%
  convert_column_names_to_snakecase() %>%
  mutate(
    delivery_start = as.POSIXct(substr(prediction_time, 1, 19), origin = "1970-01-01", tz = "CET"),
    forecast = round(price, 2),
    country = "BE"
  ) %>%
  select(delivery_start, country, forecast, actual)



# exchange rates ----------------------------------------------------------

### issue with wrong date format
exchange_rates <- get_currency_exchange_rates(start_date - days(3), end_date)

exchange_rates_tmp <- exchange_rates %>%
  filter(sourceCurrency == "GBP" & targetCurrency == "EUR") %>%
  mutate(
    day = date(as.POSIXct(timestamp, origin = "1970-01-01")),
    exchange_rate = round(rate, 6)
  ) %>%
  distinct(day, .keep_all = TRUE) %>%
  select(day, exchange_rate)


# filter the last exchange rate and add it to actual date because rate is missing for actual date
days <- distinct(data.frame(date(seq(start_date - days(3), end_date, "days"))))

colnames(days) <- "day"

exchange_rate_last <- exchange_rates_tmp$exchange_rate[nrow(exchange_rates_tmp)]

exchange_rates_processed <- exchange_rates_tmp %>%
  bind_rows(data.frame(day = date(end_date) - ddays(1), exchange_rate = exchange_rate_last)) %>%
  right_join(days, by = "day") %>%
  arrange(day) %>%
  distinct(day, .keep_all = TRUE) %>%
  fill(exchange_rate)
